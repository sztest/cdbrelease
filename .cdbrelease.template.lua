-- luacheck: push
-- luacheck: globals config readtext readbinary

readtext = readtext or function() end
readbinary = readbinary or function() end

return {
	pkg = "",
	version = "",
	type = "",
	title = "",
	short_description = "",
	tags = {},
	content_warnings = {},
	license = "MIT",
	media_license = "MIT",
	long_description = readtext("README.md"),
	repo = "",
	website = "",
	issue_tracker = "",
	forums = 0,
	maintainers = {},
	screenshots = {readbinary("screenshot.png")}
}

-- luacheck: pop
