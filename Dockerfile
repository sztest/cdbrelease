FROM alpine:3.10.2

RUN adduser -D -h /home -u 1000 app

RUN apk add --no-cache npm git lua5.1

COPY --chown=app *.js *.json *.lua LICENSE README /home/

USER app

RUN cd /home && npm i --no-audit
