"use strict";

const mylog = require("./mylog");
const config = require("./config");
const cdbfetch = require("./cdbfetch");

const sortser = (obj) =>
  JSON.stringify(Array.isArray(obj) ? obj.slice(0).sort() : obj);

const tagignore = {
  featured: true,
  spotlight: true,
};
const tagfilter = (arr) => arr.filter((x) => !tagignore[x]);

const whitespacenorm = (s) => {
  const lines = s.split("\n").map((x) => x.replace(/\b\s+\b/g, " "));
  while (lines.length && !/\S/.test(lines[lines.length - 1])) lines.pop();
  return lines.join("\n");
};

module.exports = async () => {
  const pkgurl = `/packages/${config.user}/${config.pkg}/`;
  mylog("checking existing package metadata...");
  const resp = await cdbfetch(pkgurl);

  const edits = {};
  for (let k of Object.keys(resp.body)) {
    if (k === "screenshots") continue;
    const o = resp.body[k];
    let n = config[k];
    if (n === undefined || sortser(o) === sortser(n)) continue;
    if (k === "tags" && sortser(tagfilter(o)) === sortser(tagfilter(n)))
      continue;
    if (k === "long_description" && whitespacenorm(o) === whitespacenorm(n))
      continue;
    mylog(
      JSON.stringify({
        field: k,
        old: o,
        new: n,
      })
    );
    edits[k] = config[k];
  }
  if (!Object.keys(edits).length)
    return mylog("package metadata already up to date");

  mylog(`metadata edits: ${Object.keys(edits).sort().join(", ")}`);
  if (config.dryrun) return mylog("metadata edit DRY RUN ABORT");

  mylog("updating package metadata...");
  await cdbfetch(pkgurl, "put", edits, { json: true });
  mylog("package metadata updated");
};
