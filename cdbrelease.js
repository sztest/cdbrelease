"use strict";

const fs = require("fs");
const path = require("path");
const glob = require("glob");
const archiver = require("archiver");
const mylog = require("./mylog");
const config = require("./config");
const cdbfetch = require("./cdbfetch");
const withtemp = require("./withtemp");
const spawn = require("./spawn");
const fixconf = require("./fixconf");

module.exports = async (gitexport) => {
  const relurl = `/packages/${config.user}/${config.pkg}/releases/`;

  if (!config.force) {
    mylog("checking if new version is already released...");
    const resp = await cdbfetch(relurl);
    if (
      Array.isArray(resp.body) &&
      resp.body.find((x) => x.title === config.version)
    )
      return mylog(`version ${config.version} already released`);
  }
  await withtemp(async (ziptmp) => {
    mylog("determining commit...");
    const proc = spawn(
      "git",
      [`--git-dir=${gitexport.path}`, "show-ref", config.branch],
      { stdio: ["ignore", "pipe", "inherit"] }
    );
    let buff = "";
    proc.stdout.on("data", (x) => (buff += x.toString()));
    await proc.promise;
    const comm = buff.match(/^[0-9a-f]{40}\s/);
    if (!comm) throw new Error(`no commit hash found for ${config.branch}`);
    const commit = comm[0].trim();

    const zipfile = path.join(ziptmp, "release.zip");
    const zipstr = fs.createWriteStream(zipfile);
    await gitexport(false, async (gittmp) => {
      if (config.fixconf) await fixconf(gittmp);
      mylog("archiving release...");
      const arch = archiver("zip", { zlib: { level: 9 } });
      await new Promise((res, rej) => {
        arch.on("error", rej);
        arch.on("end", res);
        arch.pipe(zipstr);
        const g = glob("**", {
          cwd: gittmp,
          dot: false,
          nodir: true,
          follow: true,
        });
        g.on("stat", (f) =>
          arch.file(path.join(gittmp, f), { name: path.join(config.pkg, f) })
        );
        g.on("end", () => arch.finalize());
      });
    });
    zipstr.end();
    zipstr.close();

    if (config.advzip) {
      let modeopts = [-4];
      if (typeof config.advzip === "number") {
        if (config.advzip <= 4) modeopts = ["-" + Math.floor(config.advzip)];
        else modeopts.push("-i", Math.floor(config.advzip));
      }
      const adv = spawn("advzip", ["-z", ...modeopts, zipfile], {
        stdio: ["ignore", "inherit", "inherit"],
      });
      await adv.promise;
    }

    const params = {
      title: config.version,
      commit: commit,
      file: { file: zipfile, content_type: "application/zip" },
    };
    if (config.dryrun)
      return mylog(`DRY RUN: package release: ${JSON.stringify(params)}`);

    mylog("uploading release...");
    await cdbfetch(`${relurl}new/`, "post", params, { multipart: true });
    mylog("release uploaded");
  });
};
