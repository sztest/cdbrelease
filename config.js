"use strict";

const path = require("path");
const minimist = require("minimist");

const proto = {};
const config = Object.create(proto);
module.exports = config;

const defaults = {
  conf: path.join(process.env.HOME, ".cdbrelease.json"),

  force: false,
  dryrun: false,

  srcrepo: undefined,
  branch: "master",

  execgit: "git",
  exectar: "tar",
  execlua: "lua5.1",
  execadvzip: "advzip",

  cdbjson: false,
  cdbjsonpath: ".cdb.json",
  cdblua: false,
  cdbluapath: ".cdbrelease.lua",

  root: "https://content.luanti.org/api",
  token: null,
  timeout: 60,

  fixconf: false,

  advzip: false,

  user: null,
  pkg: null,

  version: undefined,

  edit: false,
  type: undefined,
  title: undefined,
  short_description: undefined,
  dev_state: undefined,
  tags: undefined,
  content_warnings: undefined,
  license: undefined,
  media_license: undefined,
  long_description: undefined,
  repo: undefined,
  website: undefined,
  issue_tracker: undefined,
  forums: undefined,
  video_url: undefined,
  donate_url: undefined,
  maintainers: undefined,
  screenshots: undefined,
};

proto.allowed = {};
Object.keys(defaults).forEach((k) => (proto.allowed[k] = true));

const arrays = ["tags", "content_warnings", "maintainers", "screenshots"];

const layers = {};
const layord = "conffile gitjson gitlua cmdline".split(" ");

function set(name, obj) {
  if (!layord.find((x) => x === name))
    throw new Error("invalid config layer name");
  layers[name] = obj;

  Object.assign(config, defaults);
  layord.forEach((lk) => Object.assign(config, layers[lk] || {}));
  delete config._;
  arrays
    .filter((k) => config[k] && !Array.isArray(config[k]))
    .forEach((k) => {
      if (typeof config[k] === "string" || config[k] instanceof String)
        config[k] = `${config[k]}`.split(";");
      else config[k] = [config[k]];
    });
  return config;
}
proto.set = set;

set("cmdline", minimist(process.argv.slice(2)));
