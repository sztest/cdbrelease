"use strict";

const fsp = require("fs").promises;
const path = require("path");
const config = require("./config");
const mylog = require("./mylog");

const fixconf = async (dir, file, override) => {
  const oldconf = {};
  let multiline;
  const fp = path.join(dir, file);
  for (const line of (await fsp.readFile(fp, "utf-8"))
    .split(/\r?\n/)
    .map((x) => x.trim())
    .filter((x) => x && !x.startsWith("#"))) {
    const [k, v] = line.split("=", 2).map((x) => x.trim());
    if (multiline) {
      if (v === '"""') {
        oldconf[multiline.k] = multiline.v;
        multiline = undefined;
      } else multiline.v = `${multiline.v}${multiline.v ? "\n" : ""}${v}`;
    } else if (v === '"""') multiline = { k, v: "" };
    else oldconf[k] = v;
  }
  const newconf = Object.assign({}, oldconf, override);
  let dirty;
  for (const [k, v] of Object.entries(newconf))
    if (v !== oldconf[k]) {
      mylog(
        `modifying ${file}: ${k} = ${JSON.stringify(
          oldconf[v] || ""
        )} -> ${JSON.stringify(v)}`
      );
      dirty = true;
    }
  if (!dirty) return mylog(`${file} already correct`);
  await fsp.writeFile(
    fp,
    Object.entries(newconf)
      .map(([k, v]) => (/\n/.test(v) ? `${k} = """\n${v}\n"""` : `${k} = ${v}`))
      .join("\n")
  );
};

module.exports = async (pkgdir) => {
  const gameconf = {};
  if (config.title) gameconf.title = config.title;
  if (config.short_description) gameconf.description = config.short_description;
  const modconf = Object.assign({}, gameconf);
  if (config.pkg) modconf.name = config.pkg;
  for (const ent of await fsp.readdir(pkgdir))
    switch (ent) {
      case "game.conf":
        await fixconf(pkgdir, ent, gameconf);
        break;
      case "texture_pack.conf":
        await fixconf(pkgdir, ent, gameconf);
        break;
      case "mod.conf":
        await fixconf(pkgdir, ent, modconf);
        break;
      case "modpack.conf":
        await fixconf(pkgdir, ent, modconf);
        break;
    }
};
