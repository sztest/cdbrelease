"use strict";

const fsx = require("fs-extra");
const config = require("./config");
const mylog = require("./mylog");
const spawn = require("./spawn");

function luaser(obj) {
  if (Array.isArray(obj)) return `{${obj.map(luaser).join(",")}}`;
  if (!!obj && typeof obj === "object" && obj.toString() === "[object Object]")
    return `{${Object.keys(obj)
      .map((k) => ({
        k: /^[A-Za-z_][A-Za-z0-9_]*$/.test(k) ? k : JSON.stringify(k),
        v: luaser(obj[k]),
      }))
      .filter((x) => x.v !== "nil")
      .map((x) => x.k + "=" + x.v)}}`;
  if (typeof obj === "number" || typeof obj === "boolean") return `${obj}`;
  if (typeof obj === "string" || obj instanceof String)
    return JSON.stringify(obj);
  if (!obj || typeof obj === "function") return "nil";
  return JSON.stringify(obj.toString());
}

module.exports = async (cwd) => {
  const luahook =
    `config = ${luaser(config)}\n\n` +
    (await fsx.readFile("luahook.lua")).toString();

  mylog("loading lua metadata...");
  let buff = "";
  const hook = spawn("lua", ["-"], {
    stdio: ["pipe", "pipe", "inherit"],
    cwd,
  });
  hook.stdout.on("data", (x) => (buff += x.toString()));
  hook.stdin.write(luahook);
  hook.stdin.end();
  await hook.promise;

  try {
    const spec = JSON.parse(buff);
    config.set("gitlua", spec);
  } catch (err) {
    mylog("Raw JSON: " + buff);
    throw err;
  }
};
