"use strict";

const mylog = require("./mylog");
const config = require("./config");
const spawn = require("./spawn");
const withtemp = require("./withtemp");

async function rawexport(repodir, outdir) {
  const garch = spawn(
    "git",
    ["--git-dir=" + repodir, "archive", "--format=tar", config.branch],
    {
      stdio: ["ignore", "pipe", "inherit"],
    }
  );
  const untar = spawn("tar", ["-C", outdir, "-xf", "-"], {
    stdio: ["pipe", "inherit", "inherit"],
  });
  garch.stdout.pipe(untar.stdin);
  await garch.promise;
  await untar.promise;
}

async function gitmirror(func) {
  return await withtemp(async (tmp) => {
    mylog("cloning source repository...");
    await spawn(
      "git",
      [
        "clone",
        "--bare",
        "--depth=1",
        "-b",
        config.branch,
        config.srcrepo,
        tmp,
      ],
      {
        stdio: ["ignore", "inherit", "inherit"],
      }
    ).promise;
    async function gitexport(full, f2) {
      return withtemp(async (t2) => {
        mylog(`creating ${full ? "full" : "lean"} git export...`);
        if (full)
          await spawn("git", ["clone", "-b", config.branch, tmp, t2], {
            stdio: ["ignore", "inherit", "inherit"],
          }).promise;
        await rawexport(tmp, t2);
        return await f2(t2);
      });
    }
    gitexport.path = tmp;
    return await func(gitexport);
  });
}

module.exports = gitmirror;
