"use strict";

process.on("unhandledRejection", (e) => {
  throw e;
});

const fsx = require("fs-extra");
const path = require("path");
const config = require("./config");
const gitmirror = require("./gitmirror");
const gitlua = require("./gitlua");
const cdbfetch = require("./cdbfetch");
const cdbedit = require("./cdbedit");
const cdbrelease = require("./cdbrelease");
const cdbscreens = require("./cdbscreens");

const loadJsonConfig = async (fn, key) => {
  try {
    const conf = await fsx.readFile(fn);
    config.set(key, JSON.parse(conf.toString()));
  } catch (e) {
    if (e.code !== "ENOENT") throw e;
    console.warn(e.message || e);
  }
};

function missingConfig(...opts) {
  return new Error(
    `missing/invalid option${opts.length === 1 ? "" : "s"}: ${opts
      .map((x) => `--${x}=...`)
      .join(", ")}`
  );
}

(async () => {
  await loadJsonConfig(config.conf, "conffile");

  if (!config.srcrepo) throw missingConfig("srcrepo");
  await gitmirror(async (gitexport) => {
    if (config.cdbjson || config.cdblua)
      await gitexport(true, async (full) => {
        if (config.cdbjson)
          await loadJsonConfig(path.join(full, config.cdbjsonpath), "gitjson");
        if (config.cdblua) await gitlua(full);
      });

    if (!config.token) throw missingConfig("token");
    if (!config.user) {
      if (!config.root) throw missingConfig("root");
      const resp = await cdbfetch("/whoami/");
      if (resp.body && resp.body.username) config.user = resp.body.username;
    }

    const missing = Object.keys(config)
      .filter((k) => k !== "_")
      .filter((k) => config[k] === null)
      .sort();
    if (missing.length) throw missingConfig(...missing);
    const excess = Object.keys(config)
      .filter((k) => !config.allowed[k])
      .sort();
    if (excess.length)
      throw new Error(
        `unsupported options: ${excess.map(JSON.stringify).join(", ")}`
      );

    await Promise.all([
      config.edit && cdbedit(),
      config.edit && config.screenshots && cdbscreens(),
      config.version && cdbrelease(gitexport),
    ]);
  });
})();
