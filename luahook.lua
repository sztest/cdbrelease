-- LUALOCALS < ---------------------------------------------------------
local dofile, io, ipairs, pairs, print, string, table, tostring, type
    = dofile, io, ipairs, pairs, print, string, table, tostring, type
local io_open, string_byte, string_format, string_gsub, table_concat
    = io.open, string.byte, string.format, string.gsub, table.concat
-- LUALOCALS > ---------------------------------------------------------

function readtext(fn)
	local f = io_open(fn, "rb")
	if not f then return end
	local x = f:read("*all")
	f:close()
	return x
end

function readbinary(fn)
	local s = readtext(fn)
	local x = {}
	for i = 1, #s do
		x[#x + 1] = string_byte(s, i)
	end
	return {name = fn, data = x}
end

local ser = {}
function tojson(x)
	return (ser[type(x)])(x)
end
ser["nil"] = function()
	return "null"
end
ser["function"] = ser["nil"]
ser["userdata"] = ser["nil"]
ser["thread"] = ser["nil"]
ser["number"] = function(x)
	return tostring(x)
end
ser["boolean"] = ser["number"]
ser["string"] = function(x)
	x = string_format("%q", x)
	x = string_gsub(x, "\n", "n")
	x = string_gsub(x, "%c", function(c)
			return "\\u00" .. string_format("%02x", string_byte(c))
		end)
	return x
end
ser["table"] = function(x)
	local arr = {}
	for k, v in ipairs(x) do
		arr[k] = tojson(v)
	end
	for n in pairs(x) do
		if arr[n] == nil then
			local obj = {}
			for k, v in pairs(x) do
				obj[#obj + 1] = tojson(tostring(k))
				.. ":" .. (arr[k] or tojson(v))
			end
			return "{" .. table_concat(obj, ",") .. "}"
		end
	end
	return "[" .. table_concat(arr, ",") .. "]"
end

local data = dofile("./.cdbrelease.lua")
print(tojson(data))
