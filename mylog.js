"use strict";

const config = require("./config");

module.exports = (msg) =>
  console.warn(
    `cdbrelease[${process.pid}](${config.user || "?"}/${config.pkg || "?"}): ${msg}`
  );
