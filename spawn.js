"use strict";

const childproc = require("child_process");
const mylog = require("./mylog");
const config = require("./config");

function spawn(path, params, ...args) {
  path = config[`exec${path}`] || path;
  mylog(`> ${path} ${params.join(" ")}`);
  const proc = childproc.spawn(path, params, ...args);
  proc.promise = new Promise((res, rejraw) => {
    function rej(e) {
      rejraw(`${path} ${params.join(" ")}: ${e}`);
    }
    proc.on("error", rej);
    proc.on("exit", (code, sig) => {
      if (sig) rej("signal: " + sig);
      if (code !== 0) rej("code: " + code);
      res();
    });
  });
  return proc;
}

module.exports = spawn;
