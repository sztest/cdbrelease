"use strict";

const fsx = require("fs-extra");
const tmp = require("tmp-promise");

async function withtemp(func) {
  const tmpdir = await tmp.dir({
    mode: parseInt("700", 8),
    prefix: "cdbrelease-",
  });
  try {
    await fsx.emptyDir(tmpdir.path);
    return await func(tmpdir.path);
  } finally {
    await fsx.emptyDir(tmpdir.path);
    await tmpdir.cleanup();
  }
}

module.exports = withtemp;
